if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/usr/sbin:/usr/local/sbin:/sbin:/home/pesekcuy/.local/bin" QT_QPA_PLATFORM=wayland QT_QPA_PLATFORMTHEME=qt5ct _JAVA_AWT_WM_NONREPARENTING=1 GTK_THEME=Adwaita-dark exec sway
fi
